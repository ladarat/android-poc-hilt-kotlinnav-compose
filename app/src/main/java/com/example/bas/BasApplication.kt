package com.example.bas

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class BasApplication: Application() {
}