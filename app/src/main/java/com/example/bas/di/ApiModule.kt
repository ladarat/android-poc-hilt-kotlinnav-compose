package com.example.bas.di

import com.example.bas.data.api.HouseApi
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit

@Module
@InstallIn(SingletonComponent::class)
class ApiModule {

    @Provides
    fun provideHouseApi(retrofit: Retrofit) = retrofit.create(HouseApi::class.java)
}