package com.example.bas.di

import com.example.bas.data.api.HouseApi
import com.example.bas.domain.repository.HouseRepository
import com.example.bas.data.repository.HouseRepositoryImp
import com.example.bas.domain.repository.UserRepository
import com.example.bas.data.repository.UserRepositoryImpl
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent


@Module(includes = [ApiModule::class])
@InstallIn(ViewModelComponent::class)
object RepositoryModule {

    @Provides
    fun provideUserRepository(): UserRepository = UserRepositoryImpl()

    @Provides
    fun provideHouseRepository(api: HouseApi): HouseRepository {
        return HouseRepositoryImp(api)
    }
}