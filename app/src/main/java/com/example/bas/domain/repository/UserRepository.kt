package com.example.bas.domain.repository

import com.example.bas.domain.model.UserModel
import kotlinx.coroutines.flow.Flow

interface UserRepository {
    fun getUser(id: String): Flow<UserModel>
}