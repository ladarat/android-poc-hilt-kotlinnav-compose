package com.example.bas.domain.repository

import com.example.bas.domain.model.HouseModel
import kotlinx.coroutines.flow.Flow

interface HouseRepository {
    fun getHouse(id: String): Flow<HouseModel>
}