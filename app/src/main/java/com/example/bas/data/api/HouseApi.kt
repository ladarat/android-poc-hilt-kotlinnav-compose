package com.example.bas.data.api

import com.example.bas.data.model.HouseResponse
import retrofit2.http.GET
import retrofit2.http.Path

interface HouseApi {

    @GET("houses/{id}")
    suspend fun getHouse(@Path("id") id: String): HouseResponse
}