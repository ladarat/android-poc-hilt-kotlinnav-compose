package com.example.bas.data.model

import com.example.bas.domain.model.HouseModel
import com.google.gson.annotations.SerializedName

data class HouseResponse(
    @SerializedName("name") val name: String? = null
)

fun HouseResponse.toDomainModel(): HouseModel {
    return HouseModel(name = name)
}