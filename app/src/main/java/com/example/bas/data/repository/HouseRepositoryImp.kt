package com.example.bas.data.repository

import com.example.bas.data.api.HouseApi
import com.example.bas.data.model.toDomainModel
import com.example.bas.domain.model.HouseModel
import com.example.bas.domain.repository.HouseRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.map
import javax.inject.Inject

class HouseRepositoryImp @Inject constructor(
    private val houseApi: HouseApi
) : HouseRepository {
    override fun getHouse(id: String): Flow<HouseModel> {
        return flow {
            emit(houseApi.getHouse(id))
        }
            .map { it.toDomainModel()}
    }

}