package com.example.bas.data.repository

import com.example.bas.domain.model.UserModel
import com.example.bas.domain.repository.UserRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flowOf
import javax.inject.Inject

class UserRepositoryImpl @Inject constructor(): UserRepository {
    override fun getUser(id: String): Flow<UserModel> {
        return flowOf(UserModel(name = "Test name"))
    }
}