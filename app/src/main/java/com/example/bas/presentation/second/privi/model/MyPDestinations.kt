package com.example.bas.presentation.second.privi.model

object MyPDestinations {
    const val MY_COUPON_ROUTE = "myprivilege/mycoupon"
    const val MyPrivilegePage = "my-privilege-age"
    const val HistoryPage = "history-page"
    const val DetailPage = "detail-page"
}
