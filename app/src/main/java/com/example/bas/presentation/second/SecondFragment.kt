package com.example.bas.presentation.second

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.material3.MaterialTheme
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.platform.ViewCompositionStrategy
import androidx.fragment.app.Fragment
import com.example.bas.databinding.FragmentSecondBinding
import com.example.bas.presentation.NavArguments
import com.example.bas.presentation.model.HouseViewData

class SecondFragment : Fragment() {

    private var _binding: FragmentSecondBinding? = null
    private val binding by lazy { _binding as FragmentSecondBinding }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentSecondBinding.inflate(layoutInflater)
        return binding.root
//        return ComposeView(requireContext()).apply {
//            layoutParams = ViewGroup.LayoutParams(
//                ViewGroup.LayoutParams.MATCH_PARENT,
//                ViewGroup.LayoutParams.MATCH_PARENT
//            )
//            setContent {
//                MaterialTheme { // TODO can update by custom or remove if no need
//                    MyPrivilegeGraph()
//                }
//            }
//        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

//        arguments?.getParcelable<User>("user")?.let { user ->
//            binding.textViewName.text = user.name
//        }

        binding.composeLayout.apply {
            // Dispose of the Composition when the view's LifecycleOwner
            // is destroyed
            setViewCompositionStrategy(ViewCompositionStrategy.DisposeOnViewTreeLifecycleDestroyed)
            setContent {
                MyPrivilegeGraph()
            }
        }

        arguments?.getString("name")?.let { name ->
            binding.textViewName.text = name
        }

        arguments?.getParcelable<HouseViewData>(NavArguments.HOUSE)?.let { house ->
            binding.textViewName.text = house.name
        }
    }
}