package com.example.bas.presentation.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.bas.domain.model.UserModel
import com.example.bas.domain.repository.HouseRepository
import com.example.bas.domain.repository.UserRepository
import com.example.bas.presentation.model.HouseViewData
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onCompletion
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(
    private val userRepository: UserRepository,
    private val houseRepository: HouseRepository,
) : ViewModel() {

    private val _navigateToSecond by lazy { MutableLiveData<UserModel>() }
    val navigateToSecond: LiveData<UserModel> get() = _navigateToSecond

    private val _navigateToHouse by lazy { MutableLiveData<HouseViewData>() }
    val navigateToHouseModel: LiveData<HouseViewData> get() = _navigateToHouse

    fun executeGetHouse() {
        viewModelScope.launch {
            houseRepository.getHouse("1")
                .map { HouseViewData.from(it) }
                .onStart {

                }
                .onCompletion { }
                .catch { }
                .collect { houseViewData ->
                    navigateToHouse(houseViewData)
                }
        }
    }

    fun executeGetUser() {
        viewModelScope.launch {
            userRepository.getUser(id = "")
                .onStart { }
                .onCompletion { }
                .catch { }
                .collect { user ->
                    navigateToSecond(user)
                }
        }
    }

    fun navigateToSecond(userModel: UserModel) {
        _navigateToSecond.value = userModel
    }

    fun navigateToHouse(houseModel: HouseViewData) {
        _navigateToHouse.value = houseModel
    }
}