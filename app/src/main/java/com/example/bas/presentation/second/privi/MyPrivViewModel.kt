package com.example.bas.presentation.second.privi

import android.os.Looper
import androidx.annotation.MainThread
import androidx.lifecycle.*
import com.example.bas.presentation.second.privi.model.*
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.launch
import java.util.concurrent.atomic.AtomicBoolean

class MyPrivilegeViewModel : ViewModel() {

    val mutableLiveData = MutableLiveData<MyPrivilegeViewState>(ShowLoading)
    val mutableEvent = SingleLiveEvent<MyPrivEvent>()


    init {
        fetchData()
    }

    private fun fetchData() = viewModelScope.launch {
        // todo mock data pls update by properly
        val data = flow {
            kotlinx.coroutines.delay(2000)
            emit(
                PrivilegeList(
                    listOf(
                        MyPrivilegeModel(
                            title = "title 1",
                            campaignCode = "campaignCode",
                            couponCode = "couponCode",
                            expiryDetail = "2022-07-31T16:59:00Z",
                            image = "",
                            imageBanner = "imageBanner",
                            validDate = "2022-07-31T16:59:00Z",
                            historyType = HistoryType.UNKNOWN,
                            points = 10
                        ),
                        MyPrivilegeModel(
                            title = "",
                            campaignCode = "campaignCode",
                            couponCode = "couponCode",
                            expiryDetail = "2022-07-31T16:59:00Z",
                            image = "https://cdn.pixabay.com/photo/2022/03/08/07/08/water-7055153_960_720.jpg",
                            imageBanner = "imageBanner",
                            validDate = "2022-07-31T16:59:00Z",
                            historyType = HistoryType.UNKNOWN,
                            points = -10
                        ),
                        MyPrivilegeModel(
                            title = "title 3",
                            campaignCode = "campaignCode",
                            couponCode = "",
                            expiryDetail = "2022-07-31T16:59:00Z",
                            image = "",
                            imageBanner = "imageBanner",
                            validDate = "2022-07-31T16:59:00Z",
                            historyType = HistoryType.UNKNOWN,
                            points = null
                        ),
                        MyPrivilegeModel(
                            title = "title 4",
                            campaignCode = "campaignCode",
                            couponCode = "",
                            expiryDetail = "2022-07-31T16:59:00Z",
                            image = "https://cdn.pixabay.com/photo/2022/03/08/07/08/water-7055153_960_720.jpg",
                            imageBanner = "imageBanner",
                            validDate = "2022-07-31T16:59:00Z",
                            historyType = HistoryType.UNKNOWN,
                            points = -10
                        ),
                        MyPrivilegeModel(
                            title = "title5",
                            campaignCode = "campaignCode",
                            couponCode = "couponCode",
                            expiryDetail = "2022-07-31T16:59:00Z",
                            image = "https://cdn.pixabay.com/photo/2022/03/08/07/08/water-7055153_960_720.jpg",
                            imageBanner = "imageBanner",
                            validDate = "2022-07-31T16:59:00Z",
                            historyType = HistoryType.UNKNOWN,
                            points = 0
                        ),
                        MyPrivilegeModel(
                            title = "title 6 20% Discount today only 20% Discount today only 20% Discount today only  20% D 20% D 20% D",
                            campaignCode = "campaignCode",
                            couponCode = "couponCode",
                            expiryDetail = "2022-07-31T16:59:00Z",
                            image = "",
                            imageBanner = "imageBanner",
                            validDate = "2022-07-31T16:59:00Z",
                            historyType = HistoryType.UNKNOWN
                        ),
                        MyPrivilegeModel(
                            title = "title 7",
                            campaignCode = "campaignCode",
                            couponCode = "couponCode",
                            expiryDetail = "2022-07-31T16:59:00Z",
                            image = "",
                            imageBanner = "imageBanner",
                            validDate = "2022-07-31T16:59:00Z",
                            historyType = HistoryType.UNKNOWN
                        ),
                        MyPrivilegeModel(
                            title = "20% Discount today only 20% Discount today only 20% Discount today only  20% D 20% D 20% D",
                            campaignCode = "campaignCode",
                            couponCode = "couponCode",
                            expiryDetail = "2022-07-31T16:59:00Z",
                            image = "",
                            imageBanner = "imageBanner",
                            validDate = "2022-07-31T16:59:00Z",
                            historyType = HistoryType.UNKNOWN
                        ),
                    )
                )
            )
        }

        data.catch {
            mutableLiveData.value = ShowError
        }.onStart {
            mutableLiveData.value = ShowLoading
        }.collect {
            mutableLiveData.value = PrivilegeList(
                itemList = it.itemList
            )
        }
    }

    fun onSelect() {
        mutableEvent.value = MyPrivEvent.Navigate(MyPDestinations.DetailPage)
    }
}

class SingleLiveEvent<T> : MutableLiveData<T>() {

    private val mPending = AtomicBoolean(false)

    @MainThread
    override fun observe(owner: LifecycleOwner, observer: Observer<in T>) {

        // Observe the internal MutableLiveData
        super.observe(
            owner,
            Observer { t ->
                if (mPending.compareAndSet(true, false)) {
                    observer.onChanged(t)
                }
            }
        )
    }

    override fun setValue(t: T?) {
        mPending.set(true)

        if (Looper.myLooper() == Looper.getMainLooper()) {
            super.setValue(t)
        } else {
            super.postValue(t)
        }
    }

    /**
     * Used for cases where T is Void, to make calls cleaner.
     */
    fun call() {
        if (Looper.myLooper() == Looper.getMainLooper()) {
            value = null
        } else {
            postValue(null)
        }
    }

    companion object {
        private val TAG = "SingleLiveEvent"
    }
}

