package com.example.bas.presentation.second.privi.model

sealed class MyEvent {
    data class Navigate(val route: String) : MyEvent()
}
