package com.example.bas.presentation.second

import androidx.compose.runtime.Composable
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.example.bas.presentation.second.privi.MyPrivilegeContainer
import com.example.bas.presentation.second.privi.model.MyPDestinations

@Composable
fun MyPrivilegeGraph() {
    val navController = rememberNavController()
    NavHost(
        navController = navController,
        startDestination = MyPDestinations.MyPrivilegePage,
    ) {
        composable(MyPDestinations.MyPrivilegePage) {
            MyPrivilegeContainer(navController)
        }
        composable(MyPDestinations.HistoryPage) {
//            HistoryContainer(navController)
        }
        composable(MyPDestinations.DetailPage) {
        }
    }
}
