package com.example.bas.presentation

import android.os.Bundle
import androidx.navigation.NavType
import com.example.bas.presentation.model.HouseViewData
import com.google.gson.Gson
import java.lang.Exception

object HouseParameterType : NavType<HouseViewData?>(isNullableAllowed = true) {
    override fun get(bundle: Bundle, key: String): HouseViewData? {
        return bundle.getParcelable(key) as? HouseViewData
    }

    override fun parseValue(value: String): HouseViewData? {
        return try {
            Gson().fromJson(value, HouseViewData::class.java)
        } catch (e: Exception) {
            HouseViewData()
        }
    }

    override fun put(bundle: Bundle, key: String, value: HouseViewData?) {
        return bundle.putParcelable(key, value)
    }
}