import androidx.activity.compose.BackHandler
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import com.example.bas.presentation.second.privi.MyCouponScreen
import com.example.bas.presentation.second.privi.PrivilegeTopBar
import com.example.bas.presentation.second.privi.model.MyPrivilegeViewState

@Composable
fun MyPrivilegeScreen(
    viewState: MyPrivilegeViewState,
    onBackPress: () -> Unit,
) {
    BackHandler {
        onBackPress()
    }
    Column(
        modifier = Modifier
            .fillMaxHeight()
    ) {
        PrivilegeTopBar("My Privilege", onDismiss = {
            onBackPress()
        })

        MyCouponScreen(viewState, onCardClicked = {})
    }
}
