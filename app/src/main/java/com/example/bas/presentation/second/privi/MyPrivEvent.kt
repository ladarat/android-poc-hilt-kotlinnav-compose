package com.example.bas.presentation.second.privi

sealed class MyPrivEvent {
    data class Navigate(val route: String) : MyPrivEvent()
}
