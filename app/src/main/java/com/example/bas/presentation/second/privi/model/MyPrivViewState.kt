package com.example.bas.presentation.second.privi.model

sealed class MyPrivilegeViewState

object ShowLoading : MyPrivilegeViewState()
object ShowError : MyPrivilegeViewState()
object EmptyList : MyPrivilegeViewState()
class PrivilegeList(val itemList: List<MyPrivilegeModel> = emptyList()) : MyPrivilegeViewState()
