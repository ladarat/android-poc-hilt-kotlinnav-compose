package com.example.bas.presentation.second.privi

import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.SmallTopAppBar
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable

@Composable
fun PrivilegeTopBar(title: String, onDismiss: () -> Unit) {
    SmallTopAppBar(
        title = { Text(title) },
        navigationIcon = {
            IconButton(
                onClick = {
                    onDismiss()
                }
            ) {
                Icon(Icons.Filled.ArrowBack, contentDescription = "Localized description")
            }
        }
    )
}
