package com.example.bas.presentation.second.privi.model

data class MyPrivilegeModel(
    val campaignCode: String? = "",
    val couponCode: String = "",
    val expiryDetail: String = "",
    val image: String = "",
    val imageBanner: String = "",
    val title: String = "",
    val validDate: String = "",
    val historyType: HistoryType = HistoryType.UNKNOWN,
    val cardType: List<CardType> = emptyList(),
    val points: Int? = null
)

enum class HistoryType {
    PRIVILEGE, PRIVILEGE_POINT, TRUE_POINT, UNKNOWN
}

enum class CardType(val value: String) {
    BLACK("black"),
    BLUE("blue"),
    GREEN("green"),
    RED("red"),
    WHITE("white"),
    TRUE_CUSTOMER("no_card"),
    ALL_THAILAND("open_deal"),
    OTHER("other");

    companion object {
        fun tranValueOf(value: String): CardType {
            return when (value.lowercase()) {
                "black" -> BLACK
                "blue" -> BLUE
                "green" -> GREEN
                "red" -> RED
                "white" -> WHITE
                "no_card" -> TRUE_CUSTOMER
                "open_deal" -> ALL_THAILAND
                else -> OTHER
            }
        }
    }
}
