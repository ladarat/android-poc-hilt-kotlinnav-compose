package com.example.bas.presentation.main

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.viewModels
import androidx.navigation.createGraph
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.fragment
import com.example.bas.R
import com.example.bas.presentation.HouseParameterType
import com.example.bas.presentation.NavArguments
import com.example.bas.presentation.NavRoutes
import com.example.bas.presentation.second.SecondActivity
import com.example.bas.presentation.second.SecondFragment
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    private val viewModel: MainViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
//        observe()
//        viewModel.navigateToSecond()

        createNavGraph()
    }

    private fun createNavGraph() {
        val navController =
            (supportFragmentManager.findFragmentById(R.id.nav_host_fragment) as NavHostFragment).navController
        navController.graph = navController.createGraph(startDestination = NavRoutes.MAIN) {
            fragment<MainFragment>(NavRoutes.MAIN) {}
            fragment<SecondFragment>(NavRoutes.SECOND) {
                argument(NavArguments.HOUSE) {
                    type = HouseParameterType
                    nullable = true
                    defaultValue = null
                }
                deepLink("android-app://androidx.navigation/${NavRoutes.SECOND}/{${NavArguments.HOUSE}}")
            }
        }
    }

    private fun observe() {
        viewModel.navigateToSecond.observe(this) {
            nav()
        }
    }

    private fun nav() {
        val intent = Intent(this, SecondActivity::class.java)
        startActivity(intent)
    }
}