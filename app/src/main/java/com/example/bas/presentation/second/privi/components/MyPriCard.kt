package com.truedigital.feature.privilegehistory.ui.myprivilege.components

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Color.Companion.Green
import androidx.compose.ui.graphics.Color.Companion.LightGray
import androidx.compose.ui.graphics.Color.Companion.Red
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import coil.compose.rememberAsyncImagePainter
import com.example.bas.presentation.second.privi.model.HistoryType
import com.example.bas.presentation.second.privi.model.MyPrivilegeModel

@ExperimentalMaterial3Api
@Composable
fun MyPrivilegeList(couponsList: List<MyPrivilegeModel>, onCardClicked: () -> Unit) {
    LazyColumn {
        items(couponsList) { item ->
            Box {
                PrivilegeCard(item, onCardClicked)
            }
        }
    }
}

@ExperimentalMaterial3Api
@Composable
fun PrivilegeCard(item: MyPrivilegeModel, onCardClicked: () -> Unit) {
    Card(
        shape = RoundedCornerShape(6.dp),
        colors = CardDefaults.cardColors(
            containerColor = Color.White
        ),
        elevation = CardDefaults.cardElevation(
            defaultElevation = 5.dp
        ),
        modifier = Modifier
            .padding(
                horizontal = 16.dp,
                vertical = 6.dp,
            )
            .fillMaxWidth(),
        onClick = { onCardClicked() }
    ) {
        Column(
            modifier = Modifier
                .height(132.dp)
                .padding(16.dp),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Row(
                verticalAlignment = Alignment.Top,
            ) {
//                if (item.image.isEmpty()) {
//                    Image(
//                        painter = painterResource(id = getDefaultImage(item.historyType)),
//                        contentDescription = null,
//                        modifier = Modifier
//                            .height(60.dp)
//                            .width(106.dp)
//                    )
//                } else {
                Image(
                    painter = rememberAsyncImagePainter(item.image),
                    contentDescription = null,
                    contentScale = ContentScale.Crop,
                    modifier = Modifier
                        .height(60.dp)
                        .width(106.dp)
                        .clip(RoundedCornerShape(8.dp))
                        .aspectRatio(16f / 9f)
                )
//                }

                Column {
                    Row {
                        if (item.title.isEmpty()) {
                            Text(
                                text = getDefaultTitle(item.historyType),
                                maxLines = 2,
                                overflow = TextOverflow.Ellipsis,
                                style = TextStyle(
                                    color = Color.Black,
                                    fontWeight = FontWeight.Bold,
                                    textAlign = TextAlign.Left,
                                    fontSize = 18.sp,
                                ),
                                modifier = Modifier
                                    .padding(start = 10.dp)
                                    .weight(1f)
                            )
                        } else {
                            Text(
                                text = item.title,
                                maxLines = 2,
                                overflow = TextOverflow.Ellipsis,
                                style = TextStyle(
                                    color = Color.Black,
                                    fontWeight = FontWeight.Bold,
                                    textAlign = TextAlign.Left,
                                    fontSize = 18.sp,
                                ),
                                modifier = Modifier
                                    .padding(start = 10.dp)
                                    .weight(1f)
                            )
                        }
                    }
                    if (item.points != null) {
                        Row {
                            Text(
                                text = getDisplayTruePoints(item.points),
                                maxLines = 2,
                                overflow = TextOverflow.Ellipsis,
                                style = TextStyle(
                                    color = getColorFromPoint(item.points),
                                    fontWeight = FontWeight.Light,
                                    textAlign = TextAlign.Left,
                                    fontSize = 14.sp
                                ),
                                modifier = Modifier
                                    .padding(start = 10.dp)
                                    .weight(1f)
                            )
                        }
                    }
                }
            }
            Row(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(top = 10.dp),
                verticalAlignment = Alignment.CenterVertically,
                horizontalArrangement = Arrangement.SpaceEvenly

            ) {
//                val expiryDetail =
//                    LocalDateTime.parse(item.expiryDetail, DateTimeFormatter.ISO_DATE_TIME)
//                        .format(DateTimeFormatter.ofPattern("${DateFormatConstant.dd_MMMM_yyyy} ${DateFormatConstant.HH_mm_ss}"))
                Text(
                    text = "expiryDetail",
                    textAlign = TextAlign.Start,
                    modifier = Modifier
                        .weight(2f)
                        .padding(start = 6.dp),
                    style = TextStyle(
                        color = LightGray,
                        fontWeight = FontWeight.Bold,
                        textAlign = TextAlign.Left,
                        fontSize = 12.sp
                    ),
                )
                if (item.couponCode.isNotEmpty() && item.historyType != HistoryType.TRUE_POINT) {
                    Button(
                        colors = ButtonDefaults.buttonColors(
                            containerColor = Color.Black
                        ),
                        modifier = Modifier
                            .weight(1f)
                            .height(28.dp),
                        onClick = {
                        }, content = {
                            Text(
                                text = "Show Code",
                                style = TextStyle(
                                    color = Color.White,
                                    fontWeight = FontWeight.Bold,
                                    textAlign = TextAlign.Center,
                                    fontSize = 12.sp
                                ),
                            )
                        }
                    )
                }
            }
        }
    }
}

fun getDisplayTruePoints(points: Int): String {
    return if (points > 0) {
        "+${points} TruePoints"
    } else {
        "${points} TruePoints"
    }
}

fun getColorFromPoint(points: Int): Color {
    return if (points > 0) {
        Green
    } else {
        Red
    }
}

//fun getDefaultImage(historyType: HistoryType): Int {
//    return if (historyType == HistoryType.PRIVILEGE) {
//        R.drawable.ic_launcher
//    } else {
//        R.drawable.ic_launcher_round
//    }
//}

@Composable
fun getDefaultTitle(historyType: HistoryType): String {
    return if (historyType == HistoryType.PRIVILEGE) {
        "privilege"
    } else {
        "point"
    }
}

@ExperimentalMaterial3Api
@Preview
@Composable
fun MyPrivilegeCardPreview() {
    MyPrivilegeList(
        listOf(
            MyPrivilegeModel(
                title = "title 1",
                campaignCode = "campaignCode",
                couponCode = "couponCode",
                expiryDetail = "2022-07-31T16:59:00Z",
                image = "",
                imageBanner = "imageBanner",
                validDate = "2022-07-31T16:59:00Z",
                historyType = HistoryType.UNKNOWN
            ),
            MyPrivilegeModel(
                title = "",
                campaignCode = "campaignCode",
                couponCode = "couponCode",
                expiryDetail = "2022-07-31T16:59:00Z",
                image = "https://cdn.pixabay.com/photo/2022/03/08/07/08/water-7055153_960_720.jpg",
                imageBanner = "imageBanner",
                validDate = "2022-07-31T16:59:00Z",
                historyType = HistoryType.UNKNOWN
            ),
            MyPrivilegeModel(
                title = "title 3",
                campaignCode = "campaignCode",
                couponCode = "",
                expiryDetail = "2022-07-31T16:59:00Z",
                image = "",
                imageBanner = "imageBanner",
                validDate = "2022-07-31T16:59:00Z",
                historyType = HistoryType.UNKNOWN
            ),
            MyPrivilegeModel(
                title = "title 4",
                campaignCode = "campaignCode",
                couponCode = "",
                expiryDetail = "2022-07-31T16:59:00Z",
                image = "https://cdn.pixabay.com/photo/2022/03/08/07/08/water-7055153_960_720.jpg",
                imageBanner = "imageBanner",
                validDate = "2022-07-31T16:59:00Z",
                historyType = HistoryType.UNKNOWN
            ),
            MyPrivilegeModel(
                title = "title5",
                campaignCode = "campaignCode",
                couponCode = "couponCode",
                expiryDetail = "2022-07-31T16:59:00Z",
                image = "https://cdn.pixabay.com/photo/2022/03/08/07/08/water-7055153_960_720.jpg",
                imageBanner = "imageBanner",
                validDate = "2022-07-31T16:59:00Z",
                historyType = HistoryType.UNKNOWN
            )
        ),
        {}
    )
}

