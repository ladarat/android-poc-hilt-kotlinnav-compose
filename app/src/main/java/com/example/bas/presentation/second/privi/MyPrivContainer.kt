package com.example.bas.presentation.second.privi

import MyPrivilegeScreen
import androidx.compose.runtime.Composable
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.platform.LocalLifecycleOwner
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavHostController
import com.example.bas.presentation.second.privi.model.ShowError

@Composable
fun MyPrivilegeContainer(navHostController: NavHostController) {

    val viewModel = viewModel {
        MyPrivilegeViewModel()
    }
    val viewState = viewModel.mutableLiveData.observeAsState().value ?: ShowError
    val lifecycleOwner = LocalLifecycleOwner.current


    MyPrivilegeScreen(
        viewState = viewState,
        onBackPress = {
            viewModel.onSelect()
        }
    )
}
