package com.example.bas.presentation.main

import android.net.Uri
import android.os.Bundle
import android.view.View
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.example.bas.R
import com.example.bas.domain.model.HouseModel
import com.example.bas.domain.model.UserModel
import com.example.bas.presentation.NavRoutes
import com.example.bas.presentation.model.HouseViewData
import com.google.gson.Gson
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainFragment : Fragment(R.layout.fragment_main) {

    private val viewModel: MainViewModel by viewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observe()
//        viewModel.executeGetUser()
        viewModel.executeGetHouse()
    }

    private fun observe() {
        viewModel.navigateToSecond.observe(viewLifecycleOwner) { user ->
            navToSecond(user)
        }
        viewModel.navigateToHouseModel.observe(viewLifecycleOwner) { house ->
            navToHouse(house)
        }
    }

    private fun navToSecond(userModel: UserModel) {
        findNavController().navigate(
            R.id.action_mainFragment_to_secondFragment,
            bundleOf("user" to userModel)
        )

//        findNavController().navigate(
//           Uri.parse("myapp://second/Test2")
//        )
    }

    private fun navToHouse(houseModel: HouseViewData) {
//        findNavController().navigate(
//            R.id.action_mainFragment_to_secondFragment,
//            bundleOf("house" to houseModel)
//        )

        val houseArument = Uri.encode(Gson().toJson(houseModel))
        findNavController().navigate("${NavRoutes.SECOND}/${houseArument}")
    }
}