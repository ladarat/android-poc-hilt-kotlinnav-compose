package com.example.bas.presentation.model

import android.os.Parcelable
import com.example.bas.domain.model.HouseModel
import kotlinx.parcelize.Parcelize

@Parcelize
data class HouseViewData (
    val name: String? = null
): Parcelable{
    companion object {
        fun from(houseModel: HouseModel): HouseViewData{
            return HouseViewData(name = houseModel.name)
        }
    }
}