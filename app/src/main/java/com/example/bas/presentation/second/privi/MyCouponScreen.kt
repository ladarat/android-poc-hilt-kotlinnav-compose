package com.example.bas.presentation.second.privi

import android.widget.Toast
import androidx.compose.runtime.Composable
import androidx.compose.ui.platform.LocalContext
import com.example.bas.presentation.second.privi.model.*
import com.truedigital.feature.privilegehistory.ui.myprivilege.components.MyPrivilegeList

@Composable
fun MyCouponScreen(viewState: MyPrivilegeViewState?, onCardClicked: () -> Unit) {
    when (viewState) {
        ShowLoading -> {
            // showLoading()
            Toast.makeText(
                LocalContext.current,
                "itemList is show Loading", Toast.LENGTH_SHORT
            ).show()
        }
        is PrivilegeList -> {
            MyPrivilegeList(viewState.itemList, onCardClicked = {
                onCardClicked()
            })
        }
        is EmptyList -> {
            Toast.makeText(
                LocalContext.current,
                "itemList is empty", Toast.LENGTH_SHORT
            ).show()
        }
        is ShowError -> {
            // showError()
            Toast.makeText(
                LocalContext.current,
                "itemList is error", Toast.LENGTH_SHORT
            ).show()
        }
    }
}
